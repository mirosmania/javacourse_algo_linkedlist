package ru.javacourse.mail;


public class Starter {
    public static void main(String[] args) {
        MessageQueue mq=new MessageQueue();
        for (int i=0;i<5;i++){
            MailMessage mm=new MailMessage();
            mm.setFrom("From_"+i);
            mm.setTo("To_" + i);
            mm.setSubject("Subject_" + i);
            mm.setBody("Body_" + i);
            mq.push(mm);
        }


//Homework part1(Добавить метод, который достает сообщение по индексу и не удаляет) метод mq.getMessage
        //проверка работы Homework part1
            System.out.println(mq.getMessage(3).getSubject().equals("Subject_3"));//true
            System.out.println(mq.getMessage(0).getSubject().equals("Subject_20"));//false
            System.out.println(mq.getMessage(0).getSubject().equals("Subject_0"));//true


//Homework part2(Реализовать возможность добавлять в начало очереди)метод mq.pushToHead
        MailMessage mm=new MailMessage();
        mm.setFrom("From_"+888);
        mm.setTo("To_" + 888);
        mm.setSubject("Subject_" + 888);
        mm.setBody("Body_" + 888);
        mq.pushToHead(mm);
        //проверка работы Homework part2
            System.out.println(mq.getMessage(0).getSubject().equals("Subject_888"));//true


//Homework part3(Реализовать возможность вытаскивать из конца)метод mq.pullFromTail
        //проверка работы Homework part3
        System.out.println(mq.pullFromTail().getSubject().equals("Subject_4"));//true


//сделано на занятии(Реализовать возможность вытаскивать из начала)
        System.out.println(mq.pull().getSubject());

    }
}

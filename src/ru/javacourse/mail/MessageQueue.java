package ru.javacourse.mail;

public class MessageQueue {
    private MessageBox head=null;
    private MessageBox tail=null;
    private int size=0;


    /**
     * вставляет новое сообщение в очередь(в конец)
     * @param mailMessage
     * @return кол-во сообщений в очереди
     */
    public int push(MailMessage mailMessage) {
        MessageBox mb=new MessageBox();
        mb.setMessage(mailMessage);
        mb.setIndex(size);

        if (head == null){
            head = mb;
            tail = mb;
        }
        else{
            tail.setNextMessageBox(mb);
            tail = mb;
        }
        size++;
        return getSize();
    }


    public int pushToHead(MailMessage mailMessage){
        if(getSize()==0)
            return push(mailMessage);

        //если дошли до сюда значит очередь не пуста и в ней более одного сообщения
        int tmpSize=getSize();
        MessageBox tmpMb=head;

        //создание нового head
        MessageBox mb=new MessageBox();
        mb.setMessage(mailMessage);
        mb.setIndex(0);

        head = mb;

        tail=tmpMb;
        head.setNextMessageBox(tail);
        //увеличение индексов на 1 для сообщений идущих за head
        while (tmpSize>0){
            tmpMb.setIndex(tmpMb.getIndex() + 1);
            tail=tmpMb;
            tmpMb=tmpMb.getNextMessageBox();
            tmpSize--;
        }
        size++;
        return getSize();
    }



    public int getSize(){
        return size;
    }


    /**
     * @return вытаскивает самое старое сообщение,это сообщение из очереди исчезает
     */
    public MailMessage pull(){
        if(head!=null){
            MailMessage mm=head.getMessage();
            head=head.getNextMessageBox();
            if(head==null)
                tail=null;
            size--;
            return  mm;
        }
        return null;
    }


    /**
     * @return вытаскивает самое последнее сообщение,это сообщение из очереди исчезает
     */
    public MailMessage pullFromTail(){
        if(tail!=null){
            MailMessage mm=tail.getMessage();
            size--;
            int tmpSize=getSize();
            MessageBox tmpMb=head;
            while (tmpSize>0){
                tmpMb=tmpMb.getNextMessageBox();
                tmpSize--;
            }
            tail=tmpMb;
            if(tail==null)
                head=null;
            return mm;
        }
        return null;
    }


    /**
     * по индексу возвращает сообщение не удаляя его из очереди
     * @param index
     * @return
     */
    public MailMessage getMessage(int index){
        if(head!=null && checkIndex(index)){
/* вот здесь не понял почему в tmpMb,- head передается по значениею,а не по ссылке-ведь тут ссылка на объект,если бы по ссылке  передавалось- мое решение не работало бы,
  а тут такая удача..Я к тому что делая tmpMb.getNextMessageBox() не происходит head.getNextMessageBox(),а должно т.к tmpMb=head*/
            MessageBox tmpMb=head;
            while (tmpMb!=null){
                if(tmpMb.getIndex()==index)return tmpMb.getMessage();
                tmpMb=tmpMb.getNextMessageBox();
            }
        }
        return null;
    }


    private boolean checkIndex(int index){
        if(index<getSize() && index>=0){
            return true;
        }
        System.out.println("Сообщение под индексом "+index+" не найдено");
        return false;
    }
}


class MessageBox{
    private MailMessage message;
    private MessageBox nextMessageBox;
    private int messageBoxIndex;

    public MessageBox getNextMessageBox() {
        return nextMessageBox;
    }

    public void setNextMessageBox(MessageBox nextMessageBox) {
        this.nextMessageBox = nextMessageBox;
    }

    public MailMessage getMessage() {
        return message;
    }

    public void setMessage(MailMessage message) {
        this.message = message;
    }


    public MessageBox setIndex(int index){
        this.messageBoxIndex=index;
        return this;
    }
    public int getIndex(){
        return  messageBoxIndex;
    }
}
